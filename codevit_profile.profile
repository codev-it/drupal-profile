<?php

use Drupal\Core\Form\FormStateInterface;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: codevit_profile.profile
 * .
 */

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form.
 *
 * @noinspection PhpUnused
 * @noinspection PhpUnusedParameterInspection
 */
function codevit_profile_form_install_configure_form_alter(&$form, FormStateInterface $form_state): void {
  // Add a placeholder as example that one can choose an arbitrary site name.
  $form['site_information']['site_name']['#attributes']['placeholder'] = t('My site');

  // Pre-populate the site email.
  $form['site_information']['site_mail']['#default_value'] = 'entwicklung@codev-it.at';

  // Account information defaults
  $form['admin_account']['account']['name']['#default_value'] = 'superadmin';
  $form['admin_account']['account']['mail']['#default_value'] = 'entwicklung@codev-it.at';

  // Date/time settings
  $form['regional_settings']['site_default_country']['#default_value'] = 'AT';
  $form['regional_settings']['date_default_timezone']['#default_value'] = 'Europe/Vienna';
  // Unset the timezone detect stuff
  unset($form['regional_settings']['date_default_timezone']['#attributes']['class']);

  // Only check for updates, no need for email notifications
  $form['update_notifications']['enable_update_status_module']['#default_value'] = TRUE;
  $form['update_notifications']['enable_update_status_emails']['#default_value'] = FALSE;
}
