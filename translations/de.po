# German translation of codevit_profile profile.
#
# Copyright (c) 2018 by codev-it
#
msgid ""
msgstr ""
"Project-Id-Version: codevit_profile\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"Last-Translator: \n"
"Language: de\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Language-Team: \n"
"X-Generator: Poedit 2.2.1\n"

msgid "Home"
msgstr "Startseite"

msgid "Log in"
msgstr "Login"

msgid "Forgot Password?"
msgstr "Passwort vergessen?"

msgid "Caps Lock is enable"
msgstr "Feststelltaste aktiviert"

msgid "Create new account"
msgstr "Neuen Account erstellen"

msgid "Username or e-mail address"
msgstr "Benutzername oder E-Mail-Adresse"

msgid "Password Reset"
msgstr "Passwort zurücksetzen"

msgid "Go back to the login page."
msgstr "Zurück zur Anmeldeseite."

msgid "Password"
msgstr "Passwort"

msgid "<h2>We use cookies on this site to enhance your user experience</h2><p>You have given your consent for us to set cookies.</p>"
msgstr "<h2>Wir verwenden Cookies auf dieser Website, um Ihre Benutzererfahrung zu verbessern.</h2><p>Sie haben uns das Setzen von Cookies zugestimmt.</p>"

msgid "<h2>We use cookies on this site to enhance your user experience</h2><p>By clicking the Accept button, you agree to us doing so.</p>"
msgstr "<h2>Wir verwenden Cookies auf dieser Website, um Ihre Benutzererfahrung zu verbessern.</h2><p>Durch Klicken auf die Schaltfläche Akzeptieren erklären Sie sich damit einverstanden.</p>"

msgid "<h2>We use cookies on this site to enhance your user experience</h2><p>By tapping the Accept button, you agree to us doing so.</p>"
msgstr "<h2>Wir verwenden Cookies auf dieser Website, um Ihre Benutzererfahrung zu verbessern.</h2><p>Durch Antippen der Schaltfläche Akzeptieren erklären Sie sich damit einverstanden.</p>"

msgid "<h2>Thank you for accepting cookies</h2><p>You can now hide this message or find out more about cookies.</p>"
msgstr "<h2>Danke für das Akzeptieren von Cookies</h2><p>Sie können diese Meldung nun ausblenden oder mehr über Cookies erfahren.</p>"

msgid "Basic block"
msgstr "Basis block"

msgid "A basic block contains a title and a body."
msgstr "Ein Basisblock enthält einen Titel und einen Inhalt."

msgid "Last access"
msgstr "Letzter Zugriff"

msgid "My site"
msgstr "Meine Seite"

msgid "Add content"
msgstr "Inhalte hinzufügen"

msgid "All content"
msgstr "Alle Inhalte"

msgid "Menus"
msgstr "Menüs"

msgid "Content types"
msgstr "Inhaltstypen"

msgid "Taxonomy"
msgstr "Taxonomie"

msgid "Block layout"
msgstr "Block Layout"

msgid "Views"
msgstr "Ansichten"

msgid "Image styles"
msgstr "Bild Stile"

msgid "User interface translation"
msgstr "Übersetzung der Benutzeroberfläche"

msgid "Content language"
msgstr "Sprache des Inhalts"

msgid "Single export"
msgstr "Einzel-Export"

msgid "Performance"
msgstr "Leistung"

msgid "Cron"
msgstr "Cron"

msgid "Slideshow"
msgstr "Diashow"

msgid "You have no fields. Add some to your view."
msgstr "Sie haben keine Felder. Fügen Sie einige zu Ihrer Ansicht hinzu."

msgid "@name @type in position @position of @positionCount in @groupName button group in row @row of @rowCount."
msgstr "@name @type an Position @position von @positionCount in der @groupName-Schaltflächengruppe in Zeile @row von @rowCount."

msgid "Updating translations for JavaScript and default configuration."
msgstr "Aktualisierung der Übersetzungen für JavaScript und Standardkonfiguration."

msgid "Find and manage content"
msgstr "Inhalte finden und verwalten"

msgid "A list of new users"
msgstr "Eine Liste neuer Benutzer"

msgid "Enter a part of the block, theme or category to filter by."
msgstr "Geben Sie einen Teil des Blocks, des Themas oder der Kategorie ein, nach dem gefiltert werden soll."

msgid "Shows the user names of the most recently active users, and the total number of active users."
msgstr "Zeigt die Benutzernamen der zuletzt aktiven Benutzer und die Gesamtzahl der aktiven Benutzer an."

msgid "Builder"
msgstr "Ersteller"

msgid "Presenter"
msgstr "Moderator"

msgid "Module %module granted %permission permission to authenticated users."
msgstr "Modul %module erteilt %permission für authentifizierte Benutzer."

msgid "Only untranslated strings"
msgstr "Nur nicht übersetzte Zeichenfolgen"

msgid "Content ID"
msgstr "Inhalts-ID"

msgid "Thanks for installing Colorbox"
msgstr "Danke für die Installation der Colorbox"

msgid "Who's online"
msgstr "Wer ist online"

msgid "Flush CSS and JavaScript"
msgstr "CSS und JavaScript-Cache leeren"

msgid "Configure hidden flood control variables, like the login attempt limiters."
msgstr "Konfigurieren Sie versteckte Flood-Control-Variablen, wie die Anmeldeversuchsbegrenzer."

msgid "Configure options and text strings for the Super Login module."
msgstr "Konfigurieren Sie Optionen und Textzeichenfolgen für das Super Login-Modul."

msgid "Configure menu trails."
msgstr "Konfigurieren Sie die Menüpfade."

msgid "Configure the Admin Toolbar Tools module."
msgstr "Konfigurieren Sie das Modul Admin Toolbar Tools."

msgid "Edit user account"
msgstr "Benutzerkonto bearbeiten"

msgid "Dismiss alert"
msgstr "Alarm verwerfen"

msgid "Super Login Settings"
msgstr "Super-Login-Einstellungen"

msgid "Flood unblock"
msgstr "Flood-Sperre aufheben"

msgid "Installation of frequently used third-party modules."
msgstr "Installation häufig genutzter Drittanbieter-Module."

msgid "Accept"
msgstr "Akzeptieren"

msgid "More info"
msgstr "Mehr Informationen"

msgid "No, thanks"
msgstr "Nein, danke"

msgid "Privacy settings"
msgstr "Datenschutzeinstellungen"

msgid "Save preferences"
msgstr "Einstellungen speichern"

msgid "Timor-Leste"
msgstr "Timor-Leste"

msgid "1 place"
msgstr "1 Ort"

msgid "@count places"
msgstr "@count Orte"

msgid "Sitemap Index"
msgstr "Sitemap-Index"

msgid "Account cancellation request for [user:display-name] at [site:name]"
msgstr "Anfrage zur Konto-Löschung für [user:display-name] auf [site:name]"
