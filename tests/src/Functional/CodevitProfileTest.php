<?php

namespace Drupal\Tests\codevit_profile\Functional;

use Behat\Mink\Exception\ExpectationException;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\RequirementsPageTrait;
use Drupal\Tests\SchemaCheckTestTrait;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: CodevitProfileTest.php
 * .
 */

/**
 * Class: CodevitProfileTest.
 *
 * Tests installation profile expectations.
 *
 * @package      Drupal\Tests\codevit_profile\Functional
 *
 * @group        codevit_profile
 *
 * @noinspection PhpUnused
 */
class CodevitProfileTest extends BrowserTestBase {

  use SchemaCheckTestTrait;
  use RequirementsPageTrait;

  /**
   * {@inheritdoc}
   */
  protected $profile = 'codevit_profile';

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * An array of config object names that are excluded from schema checking.
   *
   * @var string[]
   */
  protected static $configSchemaCheckerExclusions = [];

  /**
   * The admin user.
   *
   * @var User|UserInterface|false
   */
  protected User|UserInterface|false $adminUser;

  /**
   * {@inheritdoc}
   *
   * @throws EntityStorageException
   */
  protected function setUp(): void {
    parent::setUp();

    // Create admin user.
    $this->adminUser = $this->drupalCreateUser();
    $this->adminUser->addRole('presenter');
    $this->adminUser->save();
  }

  /**
   * Tests Codevit Profile installation profile.
   *
   * @throws ExpectationException
   */
  public function testCodevitProfile() {
    // Remove rebuild cache.
    $this->resetAll();
    $this->rebuildAll();

    // Check for links on front page.
    $this->drupalGet('');
    $this->assertSession()->linkExists(t('Home'));
    $this->assertSession()->linkExists(t('Forgot password?'));
    $this->assertSession()->pageTextContains(t('Log in'));
    $this->assertSession()->pageTextContains(t('Username or e-mail address'));
    $this->assertSession()->pageTextContains(t('Password'));
    $this->assertSession()->statusCodeEquals(200);

    // Ensure that there are not pending entity updates after installation.
    $this->assertFalse($this->container->get('entity.definition_update_manager')
      ->needsUpdates(), 'After installation, entity schema is up to date.');

    // Create menu link.
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/structure/menu/manage/main/add');
    $this->assertSession()->statusCodeEquals(200);
    $this->submitForm([
      'title[0][value]' => t('Test'),
      'link[0][uri]'    => '<front>',
    ], t('Save'));

    // Verify test link.
    $this->drupalGet('');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->linkExists(t('Test'));
    $this->clickLink(t('Test'));
  }

}
